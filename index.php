<!-- 
	Serving PHP Files: (run in the terminal): 
	php -S localhost:8000 
-->

<!-- 
	
	PHP code can be included to separate the declaration of variables and functions from the HTML content.

	We will use this method to separate the declaration of variables and functions from the HTML content.

	- "index.php" for embedding php in HTML to be served and shown in our browser.
	- "code.php" for defining php variables, statements and functions.

 -->


<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S01: PHP Basic and Selection Control Structure</title>
</head>
<body>
	<!-- Variables can be used to output data in double quotes while single quotes do not. -->
	<!-- <h1>Hello World!</h1> -->
	<h1>Echoing Values</h1>
	<p><?php echo "Good day $name! Your given email is $email."; ?></p>
	<!-- 
		- Single quote can be used but concatenation is needed. 
		- dot(.) is used for concatenation
	-->
	<p><?php echo 'Good day' . ' '. $name .'!' . ' '. 'Your given email is '. $email;  ?></p>

	<p><?php echo PI; ?></p>
	<!-- Normal echoing of boolean and null variables will not make it visible to the web page. -->
	<p><?php echo "hasTravelledAbroad: $hasTravelledAbroad"; ?></p>
	<p><?php echo "spouse: $spouse"; ?></p>

	<!-- To see their types and values instead, we can use gettype() or var_dump() function -->
	<!-- gettype() is similar to typeof operator of JS -->
	<p><?php echo gettype($hasTravelledAbroad); ?></p>
	<p><?php echo gettype($spouse); ?></p>

	<!-- 
		- var_dump() function.
		- displays structured information about the variable/expressions including its type and values.
	 -->

	<p><?php echo var_dump($hasTravelledAbroad); ?></p>
	<p><?php echo var_dump($spouse); ?></p>

	<!-- Printing the whole object in the browser -->
	<p><?php echo var_dump($gradesObj); ?></p>
	<!-- If we want to print a more human-readable information about the object and array we can use print_r() -->
	<p><?php print_r($gradesObj); ?></p>

	<!-- To outpub the value of an object property, the "single arrow(->)" notation can be used -->

	<p><?php echo $gradesObj->firstGrading; ?></p>
	<p><?php echo $personObj->address->state; ?></p>

	<!-- Printing the whole array -->
	<p><?php echo var_dump($grades); ?></p>
	<p><?php print_r($grades); ?></p>

	<!-- To output and array element, we can use the usual square bracket -->

	<p><?php echo $grades[3]; ?></p>
	<p><?php echo $grades[2]; ?></p>

	<h1>Operators</h1>
	<p>X: <?php echo $x; ?></p>
	<p>Y: <?php echo $y; ?></p>

	<p>IsLegal Age: <?php echo var_dump ($isLegalAge) ?></p>
	<p>IsRegistered: <?php echo var_dump ($isRegistered) ?></p>

	<h2>Arithmetic Operators</h2>
	<p>Sum: <?php echo $x + $y; ?></p>
	<p>Difference: <?php echo $x - $y; ?></p>
	<p>Product: <?php echo $x * $y; ?></p>
	<p>Quotient: <?php echo $x / $y; ?></p>
	<p>Modulo: <?php echo $x % $y; ?></p>

	<h3>Equality Operators</h3>
	<p>Loose Equality: <?php echo var_dump($x == '500'); ?></p>
	<p>Strict Equality: <?php echo var_dump($x === '500'); ?></p>
	<p>Loose Inequality: <?php echo var_dump($x != '500'); ?></p>
	<p>Strict Inequality: <?php echo var_dump($x !== '500'); ?></p>

	<h3>Greater/Lesser Operators</h3>
	<p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
	<p>Is Greater: <?php echo var_dump($x > $y); ?></p>
	<p>Is Lesser or Equal: <?php echo var_dump($x <= $y); ?></p>
	<p>Is Greater or Equal: <?php echo var_dump($x >= $y); ?></p>

	<h2>Logical Operators</h2>
	<!-- AND Operator -->
	<p>Are All Requirements Met: <?php echo var_dump($isLegalAge && $isRegistered); ?></p>
	<!-- OR Operator -->
	<p>Are Some Requirements Met: <?php echo var_dump($isLegalAge || $isRegistered); ?></p>
	<!-- NOT Operator -->
	<p>Are Some Requirements Not Met: <?php echo var_dump(!$isLegalAge && !$isRegistered); ?></p>
	
	<h1>Function</h1>
	<p>Full Name: <?php echo getFullName("John", "D", "Smith"); ?></p>

	<h2>if-Elseif-Else</h2>
	<p><?php echo determineTyphoonIntensity(120); ?></p>

	<h2>Ternary Sample (Is Underage?)</h2>
	<p><?php echo var_dump(isUnderAge(78)); ?></p>
	<p><?php echo var_dump(isUnderAge(17)); ?></p>

	<h3>Switch</h3>
	<p><?php echo determineComputerUser(5) ?></p>
	<p><?php echo determineComputerUser(1) ?></p>

	<h2>Try-Catch-Finally</h2>
	<p><?php greeting("Hello"); ?></p>
	<p><?php greeting(25); ?></p>
</body>
</html>